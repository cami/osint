## Phishing 

[]()
#### [Lateralus](https://github.com/lateralusd/lateralus)  
Create phishing campaigns using your favourite terminal. A nice CLI tool. 

#### [GoPhish](https://github.com/gophish/gophish)
[GoPhish](https://getgophish.com ) is a phishing framework for penetration testers mainly.

#### [Lucy](https://lucysecurity.com)
Chris Hadnagy prefers it to GoPhish. More features, and more robust data collection.

#### [ShellPhish](https://github.com/suljot/shellphish)
This script uses webpages generated by SocialFish Tool (https://github.com/UndeadSec/SocialFish)

#### [King Phisher](https://github.com/rsmusllp/king-phisher)
Phishing campaign toolkit. 
