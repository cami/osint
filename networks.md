## DNS

#### How to find Subdomains

https://github.com/projectdiscovery/subfinder  
https://geekflare.com/find-subdomains/  
https://findsubdomains.com/  
https://github.com/caffix/amass  

## IP information

#### [IPinfo CLI](https://github.com/ipinfo/cli)
This is the official CLI for the IPinfo.io IP address API, allowing you to:

    Look up IP details in bulk or one-by-one.
    Look up ASN details.
    Summarize the details of up to 1000 IPs at a time.
    Open a map of IP locations for any set of IPs.
    Filter IPv4 & IPv6 addresses from any input.
    Print out IP lists for any CIDR or IP range.

#### Metabigor

[https://github.com/j3ssie/metabigor](https://github.com/j3ssie/metabigor)
